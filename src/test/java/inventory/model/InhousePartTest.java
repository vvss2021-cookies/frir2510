package inventory.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InhousePartTest {

    InhousePart inhousePart1;

    @BeforeEach
    void setUp() {
        inhousePart1 = new InhousePart(52, "Part Name", 12.35d, 12, 2, 20, 11);
    }

    @Test
    void toString_returnsValidString() {
        int machineId = 11;
        assertEquals("I,52,Part Name,12.35,12,2,20,11", inhousePart1.toString());
    }

    @Test
    void getId_returnsValidId() {
        assertEquals(52, inhousePart1.getPartId());
    }
}