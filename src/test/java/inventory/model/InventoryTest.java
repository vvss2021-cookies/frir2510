package inventory.model;

import javafx.collections.FXCollections;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InventoryTest {

    private final Product p1 = new Product(1, "n1", 2, 5, 1, 10, FXCollections.observableArrayList());
    private final Product p2 = new Product(2, "n2", 2, 5, 1, 10, FXCollections.observableArrayList());
    private final Product p3 = new Product(3, "n3", 2, 5, 1, 10, FXCollections.observableArrayList());
    private final Product pNull = new Product(0, null, 0.0, 0, 0, 0, null);
    private Inventory inventory;

    @BeforeEach
    void setUp() {
        inventory = new Inventory();
    }

    @Test
    @DisplayName("F02_TC01")
    void lookupProduct_byName_isSuccessful() {
        // Arrange
        inventory.addProduct(p1);
        // Act
        Product prod = inventory.lookupProduct("n1");
        // Assert
        assertEquals(p1.getProductId(), prod.getProductId());
        assertEquals(p1.getName(), prod.getName());
    }

    @Test
    @DisplayName("F02_TC02")
    void lookupProduct_byName_notFound() {
        // Arrange
        inventory.addProduct(p1);
        inventory.addProduct(p2);
        // Act
        Product prod = inventory.lookupProduct("n0");
        // Assert
        assertEquals(null, prod);
    }

    @Test
    @DisplayName("F02_TC03")
    void lookupProduct_byName_emptyList() {
        // Arrange
        // products list - empty
        // Act
        Product prod = inventory.lookupProduct("n0");
        // Assert
        assertEquals(pNull.getProductId(), prod.getProductId());
        assertEquals(pNull.getName(), prod.getName());
    }

    @Test
    @DisplayName("F02_TC04")
    void lookupProduct_byId_isSuccessful() {
        // Arrange
        inventory.addProduct(p1);
        inventory.addProduct(p2);
        inventory.addProduct(p3);
        // Act
        Product prod = inventory.lookupProduct("2");
        // Assert
        assertEquals(p2.getProductId(), prod.getProductId());
        assertEquals(p2.getName(), prod.getName());
    }
}