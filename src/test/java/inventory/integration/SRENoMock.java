package inventory.integration;

import inventory.model.OutsourcedPart;
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


class SRENoMock {
    private String filename = "test.txt";

    OutsourcedPart part;
    InventoryRepository repository;
    InventoryService service;


    @BeforeEach
    void setUp() {
        part = new OutsourcedPart(12, "test_part", 2.1, 2, 1, 5, "Company Name");
        if (repository == null) {
            repository = new InventoryRepository(filename);
        }
        service = new InventoryService(repository);
    }

    @Test
    void addPart_executesSuccessfully() {
        int sz = service.getAllParts().size();
        service.addOutsourcePart(part.getName(), part.getPrice(), part.getInStock(), part.getMin(), part.getMax(), part.getCompanyName());
        assertEquals(sz + 1, service.getAllParts().size());

        Part lookupPart = service.lookupPart(this.part.getName());
        service.deletePart(lookupPart);
    }

    @Test
    void lookupPart_returnCorrectPart() {
        String lookupString = "test_part";

        Part partNotFound = service.lookupPart(lookupString);
        assertEquals(null, partNotFound);

        service.addOutsourcePart(part.getName(), part.getPrice(), part.getInStock(), part.getMin(), part.getMax(), part.getCompanyName());

        Part partFound = service.lookupPart(lookupString);
        assertNotEquals(null, partFound);
        assertEquals(part.getName(), partFound.getName());

        Part lookupPart = service.lookupPart(this.part.getName());
        service.deletePart(lookupPart);
    }

}