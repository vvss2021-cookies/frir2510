package inventory.integration;

import inventory.model.OutsourcedPart;
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


class SRMockE {
    private String filename = "test.txt";

    @Mock
    OutsourcedPart mockPart;

    InventoryRepository repository;
    InventoryService service;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        Mockito.when(mockPart.getName()).thenReturn("test_part");
        Mockito.when(mockPart.getPrice()).thenReturn(2.1);
        Mockito.when(mockPart.getInStock()).thenReturn(2);
        Mockito.when(mockPart.getMin()).thenReturn(1);
        Mockito.when(mockPart.getMax()).thenReturn(5);
        Mockito.when(mockPart.getCompanyName()).thenReturn("Company Name");

        if (repository == null) {
            repository = new InventoryRepository(filename);
        }
        service = new InventoryService(repository);
    }

    @Test
    void addPart_executesSuccessfully() {
        int sz = service.getAllParts().size();
        service.addOutsourcePart(mockPart.getName(), mockPart.getPrice(), mockPart.getInStock(), mockPart.getMin(), mockPart.getMax(), mockPart.getCompanyName());
        assertEquals(sz + 1, service.getAllParts().size());

        Part part = service.lookupPart(mockPart.getName());
        service.deletePart(part);

//        verify(repository, times(1)).addPart(partToAdd);
    }

    @Test
    void lookupPart_returnCorrectPart() {
        String lookupString = "test_part";

        Part partNotFound = service.lookupPart(lookupString);
        assertEquals(null, partNotFound);

        service.addOutsourcePart(mockPart.getName(), mockPart.getPrice(), mockPart.getInStock(), mockPart.getMin(), mockPart.getMax(), mockPart.getCompanyName());

        Part partFound = service.lookupPart(lookupString);
        assertNotEquals(null, partFound);
        assertEquals(mockPart.getName(), partFound.getName());

        Part part = service.lookupPart(mockPart.getName());
        service.deletePart(part);
    }

}