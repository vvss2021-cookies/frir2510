package inventory.repository;

import inventory.model.InhousePart;
import inventory.model.OutsourcedPart;
import inventory.model.Part;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import static org.junit.jupiter.api.Assertions.*;


class InventoryRepositoryTest {
    private String filename = "test.txt";

    @Mock
    InhousePart partToDelete;

    @Mock
    OutsourcedPart partToAdd;

    InventoryRepository repository;

    @BeforeEach
    void setUp() {
        if (repository == null) {
            repository = new InventoryRepository(filename);
            MockitoAnnotations.initMocks(this);
        }
    }

//    @AfterEach
//    void tearDown(){
//        FileChannel src = new FileInputStream(filename).getChannel();
//        FileChannel dest = new FileOutputStream(filename).getChannel();
//        dest.transferFrom(src, 0, src.size());
//    }


    @Test
    void addPart() {
        Mockito.when(partToAdd.getPartId()).thenReturn(0);
        Mockito.when(partToAdd.toString()).thenReturn("O,0,part_test,2.1,1,1,1,1");
        int sz = repository.getAllParts().size();

        repository.addPart(partToAdd);
        assert(repository.getAllParts().size() == sz + 1);

        repository.deletePart(partToAdd);
        assert(repository.getAllParts().size() == sz);
    }

    @Test
    void deletePart() {
        Mockito.when(partToDelete.getPartId()).thenReturn(1);

        int sz = repository.getAllParts().size();
        repository.deletePart(partToDelete);
        assertEquals(sz, repository.getAllParts().size());

        repository.addPart(partToDelete);
        assertEquals(sz + 1, repository.getAllParts().size());

        repository.deletePart(partToDelete);
        assertEquals(sz, repository.getAllParts().size());
    }
}