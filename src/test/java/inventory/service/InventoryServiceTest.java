package inventory.service;

import inventory.model.InhousePart;
import inventory.model.Part;
import inventory.model.Product;
import inventory.repository.InventoryRepository;
import inventory.validator.Validator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.*;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


class RepositoryStub extends InventoryRepository {
    @Override
    public void addPart(Part part) {
        // do nothing
    }

    @Override
    public void addProduct(Product product) {
        // do nothing
    }
}

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class InventoryServiceTest {

    private InventoryService service;
    private RepositoryStub repositoryStub;
    private Part part1;
    private Part part2;
    private ObservableList inHousePartList;

    @BeforeEach
    void setUp() {
        // Arrange
        repositoryStub = new RepositoryStub();
        service = new InventoryService(repositoryStub);
        part1 = new InhousePart(1, "part1", 0.1, 5, 2, 10, 1);
        part2 = new InhousePart(1, "part2", 0.1, 5, 2, 10, 2);
        inHousePartList = FXCollections.observableArrayList(part1, part2);
    }

    @AfterEach
    void tearDown() {
    }

    @Timeout(value = 10, unit = TimeUnit.MILLISECONDS)
    @DisplayName("Metoda addInhousePart cu date valide ECP")
    @Order(1)
    @Test
    void TC1_ECP_Valid() {
        // Act
        String err = service.addInhousePart("piesa1", 1.0, 5, 2, 7, 1);
        // Assert
        assertEquals("", err);
    }

    @DisplayName("Metoda addInhousePart cu date valide ECP")
    @Order(2)
    @Test
    void TC7_ECP_Valid() {
        // Act
        String err = service.addInhousePart("piesa_hit", 2.0, 20, 10, 50, 10);
        // Assert
        assertEquals("", err);
    }

    @DisplayName("Metoda addInhousePart cu date non-valide ECP")
    @Order(3)
    @Test
    void TC2_ECP_Non_Valid() {
        // Act
        String err = service.addInhousePart("piesa2", 1.0, 3, 5, 7, 1);
        // Assert
        assertEquals(Validator.ERR_INVENTORY_LOWER_THAN_MIN, err);
    }

    @DisplayName("Metoda addInhousePart cu date non-valide ECP")
    @Order(4)
    @Test
    void TC3_ECP_Non_Valid() {
        // Act
        String err = service.addInhousePart("", 1.0, 5, 2, 7, 1);
        // Assert
        assertEquals(Validator.ERR_NO_NAME, err);
    }

    @DisplayName("Metoda addProduct cu date valide BVA")
    @Order(5)
    @Test
    void TC1_BVA_Valid() {
        // Act
        String err = service.addProduct("M", 1.0, 5, 2, 7, inHousePartList);
        // Assert
        assertEquals("", err);
    }

    @DisplayName("Metoda addProduct cu date valide BVA")
    @Order(6)
    @Test
    void TC5_BVA_Valid() {
        // Act
        String err = service.addProduct("nume piesa", 1.0, 1, 1, 1, inHousePartList);
        // Assert
        assertEquals("", err);
    }

    @DisplayName("Metoda addProduct cu date non-valide BVA")
    @Order(7)
    @Test
    void TC7_BVA_Non_Valid() {
        // Act
        String err = service.addProduct("piesa", 1.0, 1, 2, 4, inHousePartList);
        // Assert
        assertEquals(Validator.ERR_INVENTORY_LOWER_THAN_MIN, err);
    }

    @DisplayName("Metoda addProduct cu date non-valide BVA")
    @Order(8)
    @Test
    void TC4_BVA_Non_Valid() {
        // Act
        String err = service.addProduct("piesa", 1.0, -1, 3, 7, inHousePartList);
        // Assert
        assertTrue(err.contains(Validator.ERR_EMPTY_INVENTORY));
        assertTrue(err.contains(Validator.ERR_INVENTORY_LOWER_THAN_MIN));
    }

    @Disabled("For demonstration purposes only")
    @Test
    void skippedTest() {
        // not executed
    }
}