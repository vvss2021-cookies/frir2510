package inventory.service;

import inventory.model.InhousePart;
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import javafx.beans.value.ObservableListValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class InventoryServiceMockitoTest {

    @Mock
    private InventoryRepository repo;

    @InjectMocks
    private InventoryService service;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getAllParts() {
        Part part = mock(InhousePart.class);
        Mockito.when(repo.getAllParts()).thenReturn(FXCollections.observableArrayList(part));
        ObservableList<Part> allParts = service.getAllParts();

        Mockito.verify(repo, Mockito.times(1)).getAllParts();
        assertEquals(1, allParts.size());
    }

    @Test
    void lookupPart() {
        Part part = mock(InhousePart.class);
        when(part.getName()).thenReturn("part1");

        String searchString = "test";
        Mockito.doAnswer((Answer<Part>) invocation -> {
            Object[] arguments = invocation.getArguments();
            if (arguments != null && arguments.length == 1 && arguments[0] != null) {
                String search = (String) arguments[0];
                if(part.getName().contains(search))
                    return part;
                else
                    return null;
            }
            return null;
        }).when(repo).lookupPart(Mockito.anyString());

        Part first = service.lookupPart(searchString);
        Mockito.verify(repo, Mockito.times(1)).lookupPart(searchString);
        searchString = "part1";
        Part second = service.lookupPart(searchString);
        Mockito.verify(repo, Mockito.times(1)).lookupPart(searchString);

        assertEquals(first, null);
        assertEquals(second, part);
    }

}