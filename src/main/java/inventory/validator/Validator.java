package inventory.validator;

import inventory.model.Part;
import javafx.collections.ObservableList;

public class Validator {

    public static final String ERR_NO_NAME = "A name has not been entered. ";
    public static final String ERR_EMPTY_INVENTORY = "The inventory level must be greater or equal to 0. ";
    public static final String ERR_MIN_LOWER_THAN_ZERO = "The Min value must be greater or equal to 0. ";
    public static final String ERR_PRICE_LOWER_THAN_ZERO = "The price must be greater than 0. ";
    public static final String ERR_MIN_GREATER_THAN_MAX = "The Min value must be less than the Max value. ";
    public static final String ERR_INVENTORY_LOWER_THAN_MIN = "Inventory level is lower than minimum value. ";
    public static final String ERR_INVENTORY_HIGHER_THAN_MAX = "Inventory level is higher than the maximum value. ";
    public static final String ERR_NO_PART_IN_PRODUCT = "Product must contain at least 1 part. ";
    public static final String ERR_PRODUCT_PRICE_LOWER_THAN_PRICE_OF_PARTS = "Product price must be greater than cost of parts. ";

    private Validator() {
        // private constructor hides the implicit public constructor
    }

    /**
     * Generate an error message for invalid values in a product
     * and evaluate whether the sum of the price of associated parts
     * is less than the price of the resulting product.
     * A valid product will return an empty error message string.
     * @param name
     * @param min
     * @param max
     * @param inStock
     * @param price
     * @param parts
     * @return
     */
    public static String isValidProduct(String name, double price, int inStock, int min, int max, ObservableList<Part> parts) {
        String errorMessage = "";
        double sumOfParts = 0.00;
        for (int i = 0; i < parts.size(); i++) {
            sumOfParts += parts.get(i).getPrice();
        }
        if (name.equals("")) {
            errorMessage += ERR_NO_NAME;
        }
        if (inStock < 0) {
            errorMessage += ERR_EMPTY_INVENTORY;
        }
        if (min < 0) {
            errorMessage += ERR_MIN_LOWER_THAN_ZERO;
        }
        if (price < 0.01) {
            errorMessage += ERR_PRICE_LOWER_THAN_ZERO;
        }
        if (min > max) {
            errorMessage += ERR_MIN_GREATER_THAN_MAX;
        }
        if(inStock < min) {
            errorMessage += ERR_INVENTORY_LOWER_THAN_MIN;
        }
        if(inStock > max) {
            errorMessage += ERR_INVENTORY_HIGHER_THAN_MAX;
        }
        if (parts.isEmpty()) {
            errorMessage += ERR_NO_PART_IN_PRODUCT;
        }
        if (sumOfParts > price) {
            errorMessage += ERR_PRODUCT_PRICE_LOWER_THAN_PRICE_OF_PARTS;
        }
        return errorMessage;
    }

    /**
     * Generate an error message for invalid values in a part
     * Valid part will return an empty string
     * @param name
     * @param price
     * @param inStock
     * @param min
     * @param max
     * @return
     */
    public static String isValidPart(String name, double price, int inStock, int min, int max) {
        String errorMessage = "";
        if(name.equals("")) {
            errorMessage += ERR_NO_NAME;
        }
        if(price < 0.01) {
            errorMessage += ERR_PRICE_LOWER_THAN_ZERO;
        }
        if(inStock < 1) {
            errorMessage += ERR_EMPTY_INVENTORY;
        }
        if(min > max) {
            errorMessage += ERR_MIN_GREATER_THAN_MAX;
        }
        if(inStock < min) {
            errorMessage += ERR_INVENTORY_LOWER_THAN_MIN;
        }
        if(inStock > max) {
            errorMessage += ERR_INVENTORY_HIGHER_THAN_MAX;
        }
        return errorMessage;
    }
}
